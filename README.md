# Directus

Docker Container for Directus as a backend for the react app.

## Shell Environment Parameters

These parameters should have to be in the shell process starting the container

- KEY **Make sure to change sensitive values (KEY, SECRET, ...) in production**
- SECRET
- DB_CLIENT *Name of database Client (mysql)*
- DB_HOST *Name of DB Container (project_name-mysql)*
- DB_PORT *Database Port (3306)*
- DB_DATABASE *Database Name (dev_db_name)*
- DB_USER *Database User (dev_db_user)*
- DB_PASSWORD *Database Password (dev_db_pass)*

Admin Credential to Login Directus

- ADMIN_EMAIL *(admin@example.com)*
- ADMIN_PASSWORD *(d1r3ctu5)*

OPTIONALLY, IF CACHE IS USED IN DOCKER

- CACHE_ENABLED *boolean value (true)*
- CACHE_STORE *Name of Cache container (redis)*
- CACHE_REDIS *Port of Redis (redis://cache:**Redis Port**)*

## Arguments for Build the Container

Rather using the image of Directus, a version has been used as agruments to build the docker container to make the process stable.

- DIRECTUS_DEV_VERSION *(version of directus used)*

**The volume folder is important to use the upload or sqlite. Check the [Directus Documentation](https://docs.directus.io/getting-started/installation/docker/) for more details.**

## Usage

- directus-dev.Dockerfile *Use the Directus dev version from 'build' arguments to pull a specific Directus image*
- clear-data.sh *Clear the volume folder to reset Directus*
